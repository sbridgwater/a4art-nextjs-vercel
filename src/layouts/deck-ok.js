import React from 'react';
import _ from 'lodash';

// import styles from '../components/lightgallery.module.css';

import { Layout } from '../components';
import { markdownify } from '../utils';
import PostFooter from '../components/PostFooter';

import styles from '../sass/imports/_deck.scss';  // converted css top scss https://css2sass.herokuapp.com/

export default class Post extends React.Component {
    render() {
        const page = _.get(this.props, 'page');
        return (
            <Layout {...this.props}>
                <div className="outer">
                    <div className="inner-medium">
                        <article className="post post-full">
                            <header className="post-header">
                                <h1 className="post-title">{_.get(page, 'title')}</h1>
                            </header>
                            {_.map(page.slides, (slides,slideIdx) => (
                              <div className="post-thumbnail">
                              <img src={slides.background.image} />
                              </div>
                            ))}
                            {_.has(page, 'image') && (
                                <div className="post-thumbnail">
                                    <img src={_.get(page, 'image')} alt={_.get(page, 'title')} />
                                </div>
                            )}
                            {_.has(page, 'subtitle') && (
                                <div className="post-subtitle">
                                    {_.get(page, 'subtitle')}
                                </div>
                            )}
                            {_.has(page, 'content') && (
                                <div className="post-content">
                                    {markdownify(_.get(page, 'content'))}
                                </div>
                            )}
                            <PostFooter post={page} dateFormat='%A, %B %e, %Y'/>
                        </article>
                    </div>
                </div>
            </Layout>
        );
    }
}
