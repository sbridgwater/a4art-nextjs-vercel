import React from 'react';
import _ from 'lodash';
//import React, { Component } from 'react'

import { Layout } from '../components';
import  '../sass/imports/_deck.scss';
import  '../sass/imports/_reactbnbgallery.scss';

//import { markdownify } from '../utils';
//import PostFooter from '../components/PostFooter';

import ReactBnbGallery from 'react-bnb-gallery';

const photos = [{
  photo: "https://source.unsplash.com/aZjw7xI3QAA/1144x763",
  caption: "Viñales, Pinar del Río, Cuba",
  subcaption: "Photo by Simon Matzinger on Unsplash",
  thumbnail: "https://source.unsplash.com/aZjw7xI3QAA/100x67",
}, {
  photo: "https://source.unsplash.com/c77MgFOt7e0/1144x763",
  caption: "La Habana, Cuba",
  subcaption: "Photo by Gerardo Sanchez on Unsplash",
  thumbnail: "https://source.unsplash.com/c77MgFOt7e0/100x67",
}, {
  photo: "https://source.unsplash.com/QdBHnkBdu4g/1144x763",
  caption: "Woman smoking a tobacco",
  subcaption: "Photo by Hannah Cauhepe on Unsplash",
  thumbnail: "https://source.unsplash.com/QdBHnkBdu4g/100x67",
}];

export default class MyGallery extends React.Component {

    constructor() {
      super(...arguments);
      this.state = { galleryOpened: false };
      this.toggleGallery = this.toggleGallery.bind(this);
    }

    toggleGallery() {
      this.setState(prevState => ({
        galleryOpened: !prevState.galleryOpened
      }));
    }

    render () {
      const top = _.get(this.props, 'page');
      const page = _.get(this.props, 'page.slides');
      const images = [];
      var slides = [];
      var urls = { photo: '', caption: '', subcaption: '', thumbnail: ''};
      const renData = page.map((data, idx) => {
        if (typeof {data}.data.background !== 'undefined') {
        urls.photo = {data}.data.background.image;
        }
        // if (typeof {data}.data.notes !== 'undefined') {
        //  urls.caption = {data}.data.notes[0].children[0].text
        //  }
        if (typeof {data}.data.content !== 'undefined') {
          urls.subcaption = {data}.data.content[0].children[0].text
        }
        if (typeof {data}.data.background !== 'undefined') {
        urls.thumbnail = {data}.data.background.image;
        }
        // console.log(idx);
        // console.log(urls);
        slides[idx] = urls;
        urls = {};
        return slides;
      });

      //console.log(top.description[0].children[0].text);

      var gallery_description = "Press Button to Open Gallery";
      var gallery_title = "Gallery Title";
      if (typeof top.description !== 'undefined') {
        gallery_description = top.description[0].children[0].text;
        gallery_title = top.title;
      }
      if (typeof top.title !== 'undefined') {
        gallery_title = top.title;
      }
      return (
        <React.Fragment>
        <div className="post-subtitle">
        <div className="post-card-inside">
        <center>
        <button onClick={this.toggleGallery}>Apri galleria</button>

        </center>
        <div class="raisedbox">
        <div>

        <h3><p>{gallery_title}</p></h3>
        <p>{gallery_description}</p>
        <center><img src={slides[0].photo} /></center>
        </div>
        </div>

        </div>
        </div>
        <ReactBnbGallery show={this.state.galleryOpened} photos={slides} onClose={this.toggleGallery} />
        </React.Fragment>
      )
    }
  }
