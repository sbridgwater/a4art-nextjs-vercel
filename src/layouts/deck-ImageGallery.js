import React from 'react';
import _ from 'lodash';

import { Layout } from '../components';
import  '../sass/imports/_deck.scss';

import { markdownify } from '../utils';
import PostFooter from '../components/PostFooter';

import ImageGallery from 'react-image-gallery';

const images = [
  {
    original: 'https://picsum.photos/id/1018/1000/600/',
    thumbnail: 'https://picsum.photos/id/1018/250/150/',
  },
  {
    original: 'https://picsum.photos/id/1015/1000/600/',
    thumbnail: 'https://picsum.photos/id/1015/250/150/',
  },
  {
    original: 'https://picsum.photos/id/1019/1000/600/',
    thumbnail: 'https://picsum.photos/id/1019/250/150/',
  },
];

export default class MyGallery extends React.Component {
  render() {
    const page = _.get(this.props, 'page.slides');
    const images = [];
    var slides = [];
    var urls = { original: '', thumbnail: ''};
    const renData = page.map((data, idx) => {
      urls.original = {data}.data.background.image;
      urls.thumbnail = {data}.data.background.image;
      console.log(idx);
      console.log(urls);
      slides[idx] = urls;
      urls = {};
      return slides;
    });
    return (
      <ImageGallery items={slides}>
      </ImageGallery>
    )
  }
}
