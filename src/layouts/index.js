import landing from './landing';
import page from './page';
import blog from './blog';
import blog_category from './blog_category';
import post from './post';
import deck from './deck';

export default {
    landing,
    page,
    blog,
    blog_category,
    post,
    deck
}
