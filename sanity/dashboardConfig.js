export default {
  widgets: [
    {
      name: 'project-info'
    },
    {
      name: 'project-users'
    },
    {
    name: "AmplifyDeployButton",
    options: {
      webhookUrl:
        "https://api.render.com/deploy/srv-buogcqoti7j1a5v6afo0?key=j6A4oP_l-oA",
      title: "Deploy content changes",
      buttonText: "Deploy",
    },
    },
    { name: 'notes' },
  ]
}
